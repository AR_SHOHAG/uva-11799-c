#include<stdio.h>

int main()
{
    int T, N, k, c, s=0;
    scanf("%d", &T);

    if(T<=50){
        for(c=1; c<=T; ++c){
            scanf("%d", &N);
            int i[N];
            for(k=0; k<N; ++k){
                scanf("%d", &i[k]);
                if(i[k]>s){
                    s=i[k];
                }
            }
            printf("Case %d: %d\n", c, s);
            s=0;
        }
    }

    return 0;
}
